#pragma once
#include <fstream>
#include <format>
#include <string_view>

class Logger {
public:
	Logger() : m_out("hotloader.log", std::ios::binary) {}

	template <typename... Args>
	void log(const std::string_view format, const Args&... args) {
		m_out << std::format(format, args...) << "\n";
	}

	template <typename... Args>
	void fatal(const std::string_view format, const Args&... args) {
		m_out << "FATAL: " << std::format(format, args...) << "\n";
		exit(-1);
	}
private:
	std::ofstream m_out;
};