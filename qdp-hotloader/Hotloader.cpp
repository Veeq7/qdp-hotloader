#include "Hotloader.h"

typedef int __stdcall SFileOpenArchive_t(const char* archive_name, int priority, char flags, void** handle);
SFileOpenArchive_t* const sfile_open_archive = (SFileOpenArchive_t*) 0x004100B2;

u64 Hotloader::get_folder_time(const fs::path& path) {
    u64 result = 0;
    std::error_code error_code;
    for (auto entry : fs::recursive_directory_iterator(path, error_code)) {
        result = std::max(result, get_file_time(entry.path()));
    }
    return result;
}

u64 Hotloader::get_file_time(const fs::path& path) {
    std::error_code error_code;
    fs::file_time_type file_time = fs::last_write_time(path, error_code);
    if (!error_code) {
		return file_time.time_since_epoch().count();
    }
    return 0;
}

void Hotloader::make_directory(const fs::path& path) {
    std::error_code error_code; 
	if (fs::create_directory(path, error_code)) {
		m_logger.log("Created folder {}", path.string());
	} else if (error_code) {
		m_logger.log("Cannot create folder {}", path.string());
	}
}

void Hotloader::load_plugin(const fs::path& path) {
    HMODULE library = LoadLibraryA(path.string().c_str());
    if (library) {
        typedef BOOL WINAPI GetMPQDraftPlugin_t(IMPQDraftPlugin** lppMPQDraftPlugin);
        GetMPQDraftPlugin_t* get_mpq_draft_plugin = (GetMPQDraftPlugin_t*) GetProcAddress(library, "GetMPQDraftPlugin");
        if (get_mpq_draft_plugin) {
            IMPQDraftPlugin* mpqdraft_plugin = 0;
            get_mpq_draft_plugin(&mpqdraft_plugin);
            if (mpqdraft_plugin) {
                mpqdraft_plugin->InitializePlugin(m_mpq_server);
				m_logger.log("Loaded plugin {}", path.filename().string());
            } else {
				m_logger.fatal("MpqDraftPlugin not found!");
            }
        } else {
            m_logger.fatal("Function GetMPQDraftPlugin not found!");
        }
    }
}

void Hotloader::load_mpq(const fs::path& path) {
    void* handle = 0;
    sfile_open_archive(path.string().c_str(), m_mpq_priority, 2, &handle);
    m_mpq_priority -= 1;

    if (handle) {
        m_logger.log("Loaded mpq {}", path.string());
    } else {
        m_logger.log("Failed loading mpq {}", path.string());
    }
}

void Hotloader::create_mpq(const std::string& name, const fs::path& source_path) {
	MPQHANDLE mpq = MpqOpenArchiveForUpdate(name.c_str(), MOAU_CREATE_ALWAYS, 16 * 1024);

	std::stringstream listfile_stream;
    for (auto entry : fs::recursive_directory_iterator(source_path)) {
        if (entry.is_directory()) {
            continue;
        }

        fs::path target = entry.path().lexically_relative(source_path);
		listfile_stream << target.string().c_str() << "\n";
		MpqAddFileToArchive(mpq, entry.path().string().c_str(), target.string().c_str(), 0);
    }

	std::string listfile = listfile_stream.str();
	MpqAddFileFromBuffer(mpq, listfile.data(), listfile.size(), "(listfile)", 0);
	MpqCloseUpdatedArchive(mpq, 0);
}

void Hotloader::run() {
	if (get_folder_time("mpq") > get_file_time("temp.mpq")) {
        create_mpq("temp.mpq", "mpq");
	}
	load_mpq("temp.mpq");
	make_directory("plugins");

    std::vector<fs::path> files;
    std::error_code error_code;
    for (fs::directory_entry file : fs::directory_iterator("plugins", error_code)) {
        if (file.is_directory()) {
            continue;
        }
        if (file.path().filename().string().starts_with("_")) {
            continue;
        }
        if (file.path().extension() != ".qdp") {
            continue;
        }

        files.push_back(file.path());
    }
    std::sort(files.begin(), files.end());

    for (auto file : files) {
        load_plugin(file);
    }
}