#include <string.h>
#include <stdio.h>

#include "plugin/definitions.h"
#include "plugin/plugin.h"

#include "Hotloader.h"


BOOL WINAPI Plugin::InitializePlugin(IMPQDraftServer* lpMPQDraftServer) {
	hotloader.emplace(lpMPQDraftServer);
	hotloader->run();
	return true;
}

BOOL WINAPI Plugin::TerminatePlugin() {
	hotloader.reset();
	return true;
}

//Checks that the mpqplugin's name is within the max length set by MPQDraft
C_ASSERT(sizeof(PLUGIN_NAME) <= MPQDRAFT_MAX_PLUGIN_NAME);
//static_assert(sizeof(PLUGIN_NAME) <= MPQDRAFT_MAX_PLUGIN_NAME,
//  "The mpqplugin's name string is longer than the maximum limit set by MPQDraft");


BOOL WINAPI Plugin::Identify(LPDWORD lpdwPluginID) {
	if (lpdwPluginID == nullptr) {
		MessageBox(nullptr, "Cannot assign plugin ID", nullptr, MB_OK);
		return 0;
	}

	*lpdwPluginID = PLUGIN_ID;
	return 1;
}

BOOL WINAPI Plugin::GetPluginName(LPSTR lpszPluginName, DWORD nNameBufferLength) {
	if (lpszPluginName == nullptr) {
		MessageBox(nullptr, "Cannot assign plugin name", nullptr, MB_OK);
		return 0;
	}

	if (nNameBufferLength < sizeof(PLUGIN_NAME)) {
		MessageBox(nullptr, "Plugin name buffer is too short", nullptr, MB_OK);
		return 0;
	}

	strcpy_s(lpszPluginName, nNameBufferLength, PLUGIN_NAME);
	return 1;
}

BOOL WINAPI Plugin::CanPatchExecutable(LPCSTR lpszEXEFileName) {
	if (lpszEXEFileName == nullptr) {
		MessageBox(nullptr, "Invalid path to patch target executable", nullptr, MB_OK);
		return 0;
	}

	if (!checkStarCraftExeVersion(lpszEXEFileName))
		return 0;

	return 1;
}

BOOL WINAPI Plugin::ReadyForPatch() {
	// By default, nothing to check
	return 1;
}

BOOL WINAPI Plugin::GetModules(MPQDRAFTPLUGINMODULE* lpPluginModules, LPDWORD lpnNumModules) {
	if (lpnNumModules == nullptr) {
		MessageBox(nullptr, "Cannot assign number of modules", nullptr, MB_OK);
		return FALSE;
	}

	// By default, the mpqplugin uses no modules
	*lpnNumModules = 0;
	return 1;
}

//Checks the file version of StarCraft.exe.
//If the version is correct, returns TRUE. Otherwise, this displays appropriate
//error messages and returns FALSE.
BOOL Plugin::checkStarCraftExeVersion(LPCSTR exePath) const {
	return 1;
	/*
	BOOL result = 0;
	DWORD dummy;
	char errorMessage[500];

	const DWORD fileVersionInfoBufferSize = GetFileVersionInfoSize(exePath, &dummy);

	if (fileVersionInfoBufferSize > 0) {
		VOID* fileVersionInfoBuffer = new BYTE[fileVersionInfoBufferSize];
		VS_FIXEDFILEINFO* fileInfo;
		UINT fileInfoSize;

		if (GetFileVersionInfo(exePath, NULL, fileVersionInfoBufferSize, fileVersionInfoBuffer)
			&& VerQueryValue(fileVersionInfoBuffer, "\\", (LPVOID*)&fileInfo, &fileInfoSize)) {
			//The FileVersion of StarCraft.exe must be 1.16.1.1
			const WORD requiredVersion[4] = {1, 16, 1, 1};
			const WORD currentVersion[4] = {
			  HIWORD(fileInfo->dwFileVersionMS), LOWORD(fileInfo->dwFileVersionMS),
			  HIWORD(fileInfo->dwFileVersionLS), LOWORD(fileInfo->dwFileVersionLS)
			};

			if (requiredVersion[0] == currentVersion[0]
				&& requiredVersion[1] == currentVersion[1]
				&& requiredVersion[2] == currentVersion[2]
				&& requiredVersion[3] == currentVersion[3]) {
				result = 1;
			} else
				if (currentVersion[0] == 1
					&& currentVersion[1] == 0
					&& currentVersion[2] == 0
					&& currentVersion[3] == 2) {
					sprintf_s(
						errorMessage,
						"Error: GPTP cannot patch %s"
						"\n"
						"\n StarEdit may be able to be loaded, but the plugin features"
						"\n cannot connect, so the mod is more likely to be incompatible"
						"\n with the map editor than mods not using GPTP.",
						exePath
					);
				} else {
					sprintf_s(errorMessage,
						"Error: Cannot patch %s"
						"\n"
						"\nThis plugin (" PLUGIN_NAME ") is incompatible with the current version of StarCraft."
						" The game will still be loaded, but no plugin features will be available."
						"\n"
						"\nCurrent StarCraft version: %hu.%hu.%hu.%hu"
						"\nRequired StarCraft version: %hu.%hu.%hu.%hu",
						exePath,
						currentVersion[0], currentVersion[1], currentVersion[2], currentVersion[3],
						requiredVersion[0], requiredVersion[1], requiredVersion[2], requiredVersion[3]);
				}
		} else {
			sprintf_s(errorMessage, "Cannot retrieve version information from:\n  %s", exePath);
		}
		delete[] fileVersionInfoBuffer;
	} else {
		sprintf_s(errorMessage, "Cannot retrieve version information size from:\n  %s", exePath);
	}

	if (!result)
		MessageBox(nullptr, errorMessage, nullptr, MB_OK);
	return result;
	*/
}

BOOL WINAPI Plugin::Configure(HWND hParentWnd) {
	/*
	 *  You can't really change the version from
	 *  here with the current format.  However, you
	 *  can check the version and probably set any
	 *  globals.
	 */

	MessageBox(
		hParentWnd,
		PLUGIN_NAME " (ID: " STR(PLUGIN_ID) ")"
		"\nCompiled by Veeq7"
		"\nveeq72@gmail.com"
		"\nBuilt on " __DATE__ " " __TIME__
		"\nGLHF",
		PLUGIN_NAME,
		MB_TASKMODAL
	);

	return 1;
}

static Plugin mpqplugin;

BOOL WINAPI GetMPQDraftPlugin(IMPQDraftPlugin** mpq_draft_plugin) {
	*mpq_draft_plugin = &mpqplugin;
	return 1;
}

BOOL WINAPI DllMain(HINSTANCE module_handle, DWORD reason, LPVOID reserved) {
	switch (reason) {
		case DLL_PROCESS_ATTACH:
			break;

		case DLL_PROCESS_DETACH:
			break;

		case DLL_THREAD_ATTACH:
			break;

		case DLL_THREAD_DETACH:
			break;
	}

	return 1;
}