#pragma once
#include <optional>
#include <functional>
#include <filesystem>

#define NOMINMAX
#include "SFmpqapi/SFmpq_static.h"

#include "plugin\plugin.h"
#include "Logger.h"

namespace fs = std::filesystem;

typedef unsigned long long u64;

class Hotloader {
public:
	Hotloader(IMPQDraftServer* mpq_server) : m_mpq_server(mpq_server) {} 
	void run();

private:
	u64 get_folder_time(const fs::path& path);
	u64 get_file_time(const fs::path& path);
	void make_directory(const fs::path& path);
	void load_plugin(const fs::path& path);
	void load_mpq(const fs::path& path);
	void create_mpq(const std::string& name, const fs::path& source_path);

private:
	int m_mpq_priority = 500'000;
	IMPQDraftServer* m_mpq_server;
	Logger m_logger;
};

inline std::optional<Hotloader> hotloader;