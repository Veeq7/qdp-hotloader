# QDP Hotloader

Firegraft .qdp plugin which allows to hotload other .qdp plugins, it loads them on startup and should also load/reload live.

Note: Live load/reload feature requires special approach to programming your plugin. Live load demands not depending on starcraft startup function. Live reload demands that plugin supports proper termination cleanup code, which is not currently a common practice.

How to build:
- Open solution within VS2019, build it in release mode. Plugin will be placed in out folder.

How to use:
- Add plugins that you want to hotload in plugins folder (relative to mod path).
- Don't add plugins you are hotloading in firegraft itself.
- Plugins can be marked as unused by prefixing them with underscore.

Compatibility:
- Plugin uses mpqdraft plugin format, but was written with and tested for firegraft in mind. Mpqdraft might work, but I don't guarantee it.
- Plugin is targeting SC 1.16.1. However it will probably work on other firegraft supported versions. Basically the same deal as above.

Possible Plans:
- Add mpq folder hotloading.

Let me know when you find any bugs, I will look into them.